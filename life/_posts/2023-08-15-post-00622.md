---
layout: post
title: "시흥 롯데캐슬 시그니처 특별공급 경쟁률 주변환경 공간VR 유상옵션 분석"
toc: true
---

 

## 시흥 롯데캐슬 시그니처
 7월 10일 특별공급이 진행된 시흥 롯데캐슬 시그니처 청약접수 결과, 시흥 롯데캐슬 시그니처 1블록 608가구 모집에 311건이 접수되어 평균경쟁률 0.51대 1, 시흥 롯데캐슬 시그니처 2블록 441가구 모집에 219건이 접수되어 0.5대 1을 제각각 기록했다.
두 다만 싹 최다청약접수는 신혼부부 특별공급으로 1블록 159건, 2블록 107건이 접수되었으며, 차순으로는 생애최초 특별공급으로 1블록 126건, 2블록 96건이 접수되었다.
7월 11일에는 일반공급 1순위가 진행되며, 경기도, 서울특별시, 인천광역시 거주하는 만 19세이상 성년자라면 누구나 청약가능하며, 주택수와 재당첨제한과는 무관하며 청약통장 가입기간 12개월 이상이면 1순위 청약신청이 가능하다.

 

 

### 1. 일반공급
 ◆ 시흥 롯데캐슬 시그니처 1블록
 1. 경기도 시흥시 은행동 248-30번지 일원
 2. 공동주택(아파트) 지하 3층, 지상 49층, 8개동, 총1,230세대
 3. 2027년 7월 입주예정(정확한 입주일자는 추후 통보함)

 

 ◆ 시흥 롯데캐슬 시그니처 2블록
 1. 경기도 시흥시 은행동 288-1번지 일원
 2. 공동주택(아파트) 지하 3층, 지상 49층, 6개동, 총903세대
 3. 2027년 7월 입주예정(정확한 입주일자는 추후 통보함)

 

 

 ◆ 일반공급 신청자격
입주자모집공고일(2023.06.30.) 시방 시흥시에 거주하거나 수도권(경기도, 서울특별시, 인천광역시) 지역에 거주하는 만19세 이상인 강아지 내지 세대주인 미성년자(자녀양육, 형제자매부양) 허리 입주자저축 순위별 자격요건을 갖춘 자(국내에서 거주하는 재외동포 재외국민 외국국적 동포 및 호로 포함)를 대상으로 주택형별 청약순위 별반 청약 접수가 가능함.
◆ 1순위 청약자격요건(모두 충족시)
1. 성년자 누구나 청약가능 : 만 19세이상, 세대주 및 세대원(1세대 2인이상 중복청약 가능)
2. 유주택자 청약가능 : 주택수와 상관없이 여러 채 갖고 있어도 청약가능(단, 특별공급 제외)
3. 재당첨제한 無 : 기존 당첨사실이 있어도 청약가능
4. 경기도, 서울특별시, 인천광역시 거주자 가능 : 동일 순위 내 경쟁 발생시 시흥시 거주신청자 우선
5. 청약통장 가입기간 12개월 : 지역별 예치금 충족시 청약가능
 

 일반공급 1순위 : 7월 11일
일반공급 2순위 : 7월 12일
당첨자발표 : 7월 19일(1블록)/20일(2블록)
계약체결 : 8월 1일~4일
*해당지역 : 시흥시 거주
*기타지역 : 경기, 서울, 인천 거주

 

 

### 2. 시흥 롯데캐슬 시그니처 주변환경

 ※ 학군배정사항
- 초등학교 : 검바위초로 배치 예정임
- 중학교 : 소래중학군 내 학교(대흥중, 소래중, 신천중, 시흥은행중, 은계중)로 배치 예정임
- 초등학교 통학구역 및 중학교 학교군(군)은 경기도시흥교육지원청 학생배치계획에 의거 조정될 요행 있으며, 중학교는 학군 낌새 지망별 컴퓨터 추첨에 의하여 배정(입학)하고 있으므로 거주지 인근 중학교로의 배정(진학)이 어려울 수 있음
 

 <시흥 롯데캐슬 시그니처 1블록>
 ※ 부대복리시설 : 관리사무소, 작은도서관, 경로당, 어린이집, 주민공동시설, 공동작업장, GX룸, 피트니스클럽, 실내골프클럽 등
⦁ 모처럼 서측에는 폭 20m도로, 다만 남측에는 구역 17.5m도로, 기껏해야 동측에는 구간 16m도로가 신설될 예정임. 불과시 주위 또 및 대지경계선 밖 도로는 단지내 소유가 아니며, 실시공 시 형태, 높이, 레벨계획 등이 상이할 수 있음.
⦁ 단지 북측에는 근린공원(기부채납)이 위치하고, 단지내 소유가 아니며 계획이 대목 변경 될 고갱이 있음.
⦁ 단지 서측에는 최고높이 47층의 시흥신천역한라비발디, 동측에는 최고높이 5층의 철산아파트, 남측에는 최고높이 49층의 높이의 C-2 블록이 위치하고 있음.
⦁ 조경 및 옹벽 계획등은 주택과 옹벽으로의 이격거리를 표기하여야 함(102동 - 옹벽과 최소이격거리 : 약6.0m, 옹벽최고높이 : 약2.5m, / 103동 - 옹벽과 최소이격거리 : 약2.5m, 옹벽최고높이 : 약2.5m)향후 경미한 사업승인변경을 통해 개선된 설계안으로 시공예정이며, 분양홍보물과 상이하게 당지 시공 될 예정임.


 <시흥 롯데캐슬 시그니처 2블록>
 ※ 부대복리시설 : 관리사무소, 작은도서관, 경로당, 어린이집, 주민공동시설, 공동작업장, GX룸, 피트니스클럽, 실내골프클럽 등
· 겨우 북측에는 영역 17.5m도로, 서측에는 범주 20m도로, 어렵사리 남측에는 한도 11m도로, 단 동측에는 영역 16m도로가 신설될 예정임. 단지 행동환경 가항 및 대지경계선 밖주인 도로는 단지내 소유가 아니며, 실시공 시 형태, 높이, 레벨계획 등이 상이할 수 있음.
· 단지 서측에는 최고높이 47층의 시흥신천역한라비발디, 동측에는 최고높이 5층의 연희아파트, 남측에는 최고높이 17층의 높이의 삼성홈타운아파트, 북측에는 최고높이 49F 높이의 C-1 블록이 위치하고 있음
· 조경 및 옹벽 계획등은 주택과 옹벽으로의 이격거리를 표기하여야 함(205동- 옹벽과 최소이격거리 : 약4.0m, 옹벽최고높이 : 약3.0m, / 206동- 옹벽과 최소이격거리 [시흥롯데캐슬](https://inconclusivepart.com/life/post-00052.html){:rel="nofollow"} : 약3.0m, 옹벽최고높이 : 약3.0m)향후 경미한 사업승인변경을 통해 개선된 설계안으로 시공예정이며, 분양홍보물과 상이하게 현장 시공 될 예정임.

 

### 3. 시흥 롯데캐슬 시그니처 유상옵션

 ⦁ 주방 스타일업 옵션선택 가요 엔지니어드스톤 주방벽, 상판이 설시 되며, 옵션 미선택 곡 주방벽은 국산타일(300*600), 주방상판은 인조대리석(MMA)이 설치됨.
⦁ 거실 스타일업 벽부형 옵션(거실,복도,쇼파뒷벽 시트판넬마감) 미선택시 거실 아트월 제외부위는 벽지마감임. 또한, 시트판넬의 나누기 위치는 변경될 생명 있으며 견본주택과 일부 상이할 수 있음.
⦁ 거실 스타일업 바닥형(거실, 복도, 주방) 외산바닥타일(이태리산) 옵션 미선택시 전실 강마루 마감임.
⦁ 84A : 복도팬트리특화 옵션선택시 팬트리, 장식장, 주방우물천장(직,간접조명)이 설치되며, 유상옵션 미선택시 복도팬트리 공틀만 설치됨.
⦁ 84A : 안방 붙박이장 옵션선택시 슬라이딩 대형 붙박이장이 설치됨.
⦁ 84C : 안방 대형드레스룸 옵션선택시 드레스룸 유리슬라이딩도어 후면은 슬라이딩도어 간섭으로 수납이 불가함.
⦁ 84C : 안방 대형드레스룸 옵션선택시 안방 드레스룸 전면벽면 인테리어 시트판넬, 유리슬라이딩도어, 드레스룸내부 시스템화장대가 설치되며, 유상옵션 미선택시 벽면 벽지마감 및 가구 공틀도어, 스탠딩화장대가 기본으로 설치됨.
⦁ 거실 스타일업 천장형 옵션(조명옵션) 선택 가요곡 거실 우물천정 조명(직,간접)이 제공되며, 세대형별 우물천장의 크기,설치위치가 상이할 핵 있음. 또한, 거실 우물천장 조명(직,간접) 설치 시 기본 직부등은 설치되지 않음.
⦁ 비스포크 냉장고,김치냉장고 옵션 미선택 단시 상부장이 있는 오픈형 냉장고장이 확장시 설치됨.
⦁ 비스포크 냉장고,김치냉장고 옵션선택 가요 수납 효율을 높여주는 수납톨장이 같이 제공되며, 유상옵션 미선택시 상부장이 있는 오픈형 냉장고장이 확장시 설치됨.
⦁ 식기세척기 옵션 미선택 시 해당 위치에 수납장이 제공됨.
⦁ 하이브리드쿡탑 옵션 미선택 구가 3구 가스쿡탑이 설치됨.
 

 

 시흥 롯데캐슬 시그니처 84A㎡
발코니확장 1620만원
시스템에어컨 500만원/800만원
냉장고장 옵션 650만원
(비스포크4도어냉장고+비스포크3도어김치냉장고(빌트인팬트리+가구판넬벽 포함))
식기세척기 130만원
의류관리기 150만원
하이브리드쿡탑 55만원
현관 중문 180만원
거실스타일업① 천장형 350만원
(거실 우물천장, 직간접조명)
거실스타일업② 바닥형 455만원
(거실/복도/주방 바닥타일)
거실스타일업③ 벽부형 140만원
(거실아트월 세라믹타일, 소파 뒷벽 픽쳐레일, 거실,복도,쇼파 뒷벽 시트판넬)
복도팬트리 특화 650만원
(팬트리+장식장+주방우물천장(직간접),
가구판넬 복도벽, 시트판넬 주방벽)
복도팬트리 내부 시스템선반 130만원
주방 스타일업① 250만원
(주방 벽/상판 ENG)
안방 붙박이장 445만원
침실2 붙박이장 113만원
 

 

 시흥 롯데캐슬 시그니처 84C㎡
발코니확장 1740만원
시스템에어컨 500만원/800만원
냉장고장 옵션 650만원
(비스포크4도어냉장고+비스포크3도어김치냉장고(빌트인팬트리+가구판넬벽 포함))
식기세척기 130만원
의류관리기 150만원
하이브리드쿡탑 55만원
거실스타일업① 천장형 350만원
(거실 우물천장, 직간접조명)
거실스타일업② 바닥형 455만원
(거실/복도/주방 바닥타일)
거실스타일업③ 벽부형 200만원
(거실아트월 세라믹타일, 소파 뒷벽 픽쳐레일, 거실,복도,쇼파 뒷벽 시트판넬)
복도팬트리 내부 시스템선반 130만원
주방 스타일업① 275만원
(주방 벽/상판 ENG)
주방 스타일업④ 125만원
(장식장(주방 벽/상판))
안방 대형드레스룸 185만원
침실2 붙박이장 113만원
 

 시흥 롯데캐슬 시그니처 홈페이지
 https://www.lottecastle.co.kr/APT/AT00393/main/index.do
 
 ,주소:
경기 시흥시 은행동 248-30

 

 2023.06.30 - 시흥 롯데캐슬 시그니처 모집공고 입지 분양가 은행동시세 청약분석
 

 

 

