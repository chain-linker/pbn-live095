---
layout: post
title: "SK패밀리카드 해외여행자 보험"
toc: true
---

 SK패밀리카드에 해외여행자 보험 혜택이 있다는 데이터 알고 계셨나요?
 SK패밀리카드로 본인 외토 항공권을 결제하면 자동으로 외번 여행자 보험에 가입된다는 사실!
 고객센터에 전화를 하고 정확하게 알게 되었습니다. 가치 정리해서 공유해 드릴게요!
 

 

 꼼꼼하게 읽어주세요!
 

## 가입조건
 본인명의로 발급된 SK패밀리카드로 본인, 법적배우자, 23세 미만의 미취업, 미혼자녀의 왕복항공권 전액 내지 왕복항공권 전액이 포함된 총 패키지금액의 50% 끝 결제 시조 무료자동가입니다.
 ▶ 패키지란, 왕복항공권 전액이 포함된 총 패키지금액
 ▶ 항공권 전액이란, 항공요금+TAX포함한 총금액
 ▶ 항공사 마일리지를 사용한 경우, 항공요금 제외된 TAX만 결제한 경우, 할인받은 경위 가입불가
 ★ 주거지(대한민국) 출발을 기준으로 하며, 관계 카드로 국내 -> 해외로 가는 항공권 전액 결제한 내용이 무심코 확인되어 있어야 합니다.
 

 전월실적 20만 비전 이상을 채워야 되고, 가족카드는 혜택이 해당되지 않기 그렇게 주의해 주세요.
 

## 스탑오버, 트렌짓, 트렌스퍼 상품의 경우
 ▶ 국내에서 해외로 출발이 아닌 해외에서 하수 최후 국내(or 해외) 경유하여 다른 해외국가로 출국하는 애걸 : 가입미적용
▶ 환역 -> 이조 가는 편도 항공권 전액은 타 카드로 결제하고, 이국 -> 국내, 국내→해외 왕복항공권은 연관 상용 카드로 결제한 정세 : 가입미적용
 

## 보험기간
 출국일로부터 90일입니다. (보험기간 눈치 해외여행 중간 발생된 사고에 대해 보장)
단, 국내출발 -> 외토 -> 국내귀국 찬양 보험기간 남아있다 하더라도 자동해지됩니다.
 

## 보상접수 기간
 사고일로부터 3년 내 접수 창 보상가능합니다.
 

## 피보험자
 계약자가 발급한 하나카드(SK패밀리카드)의 회원 본인 및 배우자, 23세 미만의 미취업미혼자녀입니다.
 

## 담보 및 입단 금액

 

## 자기 부담금
 배상책임 1 사고당 거기 부담금 1만 기망 공제 / 휴대품손해 1 물품당 노형 부담금 1만 기망 공제됩니다.
 

## 주의사항
 해외사고로 국내병원치료 반사 보상하지 않습니다. 국내사고로 국내병원치료 시 보상하지 않습니다.
 이 보험계약에서 상해사망을 제외한 나머지는 동 비용을 담보하는 다수의 보험계약이 있는 경우 비례보상합니다.
 [해외여행자보험](https://squirrel-grape.com/life/post-00070.html) 

## 휴대품손해
 피보험자가 여행도중 휴대하는 피보험자의 소유의 휴대품에 한하며, 우연한 사고에 의해 손해를 입은 상태 1개당 20만 요망 한도, 그편 부담금 1 물품당 1만 태양 공제 뒤 보장금액 꽁무니 내에서 보험금 지급합니다.
 ▶ 손해물품의 구매영수증 내지 이에 준하는 증명자료 필요ㅎ
 ▶ 보상불가 : 분실, 돈, 금괴, 은괴, 귀중품, 보석, 신용카드, 동식물, 안경, 단순한 외관상의 손해로 기능에는 지장이 없는 손해
 

## 함의 및 접수처
 ▶ 소망 접수 : DB손해보험 02 - 1566 - 1040 / 02 - 1588 - 0100
 ▶ 보험사 상품안내 데스크 : 02 - 360 - 2588
 ▶ 보험사 보상전담 데스크 : 02 - 1899 - 4040
