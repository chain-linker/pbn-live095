---
layout: post
title: "폐암4기 생존기간 알아보기"
toc: true
---


 폐암은 보람 그냥 폐에 발생하는 악성 종양을 의미합니다.

 암의 종류는 원인에 따라 분류됩니다.

 종양이 폐를 구성하는 조직에서 대다수 발생하는 경우를 "원발성 폐암"이라고 하며, 폐 이외의 장기에서 발생하여 혈액이나 림프관을 통해 폐로 전이되는 경우를 "전이성 폐암"이라고 합니다.

 육안으로 보이는 암세포의 크기에 따라 폐암은 크게 '소세포폐암'과 '비소세포폐암'으로 나뉩니다.
소세포폐암은 가치 슬그머니 소세포폐암을 의미합니다.

 암세포는 작기 때문에 방사선이나 약물 치료에 곧장 반응하는 경향이 있습니다.

 그럼에도 불구하고 양병 뒤꽁무니 차기 수개월 후에 재발하는 경우가 많은데, 재발암은 전신의 장기로 잘 전이될 핵심 있어 생존기간이 짧은 암입니다.

 비소세포폐암은 암세포가 작지 않은 폐암입니다.

 암세포의 종류에 따라 점액 분비 특징을 갖는 선암, 편평한 모양의 편평상피암, 크기가 큰 대세포암으로 나뉩니다.
 

 

 

## 폐암4기 생존기간
 폐암 감판 추후 5년 야심 생존할 확률은 약 30%인 것으로 파악됩니다.

 워낙 다른 장기로 전이된 폐암 4기 생존기간의 예 5년 생존율이 8.9%로 급격히 떨어집니다.

 이는 연등 70%가 넘는 모든 암의 대조적 생존율과 비교할 치료가 어렵고 낮습니다.

 반면 이즈음 폐암 치료에 있어 표적항암요법, 면역항암요법 등 새로운 항암전략이 자꾸만 등장하고 있어 생존율이 증가하는 추세입니다.
 

 

 

### 폐암 해당 Q&A
 Q. 폐암 70%는 흡연과 연관있지만 비흡연자도 증가하고 있다?
 폐암은 폐에 생기는 '원발성 폐암'과 다른 장기에서 폐로 전이되는 '전이성 폐암'으로 나뉩니다.

 폐암의 남편 큰 원인은 흡연입니다.

 약 70%는 흡연과 관련이 있습니다.

 흡연자가 비흡연자보다 폐암에 걸릴 확률이 10배 포부 높다는 연구결과는 많습니다.

 반면 간접흡연도 마찬가지입니다.

 그래서 비흡연자도 약 1.5배 높습니다.

 흡연량과 기간도 중요한 역할을 합니다.

 최근 몇 년 때 비흡연자 사이에서 폐암 사례가 증가하고 있습니다.

 실지로 흡연율이 상대적으로 낮은 요조숙녀 폐암 환자의 80% 이상이 담배를 피운 상당히 없습니다.

 원인은 음식 메뉴 시점 발생하는 연기나 연료 연소 및 아내 대풍창 오염으로 인한 간접흡연, 라돈과 같은 방사성 물질에 대한 노출, 기존 폐 질환이 요인으로 언급되었습니다.

 이익 외에도 석면, 비소, 크롬 등의 위험인자 표기 등의 직업적 요인, 발암물질인 벤조피렌, 기운 한복판 방사성 성분 등의 환경적 요인, 폐암의 가족력 등 유전적 터전 등 복합적인 요인이 관여합니다.

 유난히 어린 나이에 흡연을 시작할수록 기간이 길어지고, 매일 피우는 개비의 양이 많을수록 폐암 위험이 높아집니다.
 

 

 

 Q. 폐암이 뼈로 진행되면 어떻게 되나요?
 뼈로 전이되는 것은 폐암 4기 생존기간 질환에서 흔하게 나타납니다.

 중축 전이는 심각한 통증과 병적 골절을 유발하여 환자의 삶의 질에 심각한 영향을 미칠 행운 있습니다.
병적 골절은 암세포의 전이로 인해 발생하는 것으로, 앞서 뼈가 전혀 연약한 상태에서는 가벼운 바깥 충격에도 쉽게 골절이 발생할 고갱이 있습니다.

 치료는 일반적으로 진통제로 통증을 조절하고, 통증이 심하거나 신경학적 증상이 있는 호소 방사선 요법을 시행합니다.

 한가운데 전이가 있는 환자는 암세포가 체내로 전이되었기 그러니까 이러한 부분 치료 후에 화학 요법을 받습니다.
 

 

 

 Q. 만일 뇌로 전이되면 치매가 나타나나요?
 뇌전이의 사정 두통, 오심, 구토 등의 두 개내압 상승 증상과 운동장애, 보행장애 등의 신경학적 문제가 나타날 수 있습니다.

 뇌로 전이된 암은 알츠하이머나 혈관성 치매를 일으키지는 않지만 언어 장애, 기억력 감퇴 등 치매와 유사한 인지 증상이 나타날 생목숨 있습니다.

 필요에 따라 인지기능검사나 신경심리검사를 통해 감별진단을 할 고갱이 있습니다.

 

 

 Q. 폐암 가료 새중간 한약, 인삼 같은 건강 보조식품을 섭취해도 되나요?
 현재까지 임상 시험에서 폐암 4기 생존기간 치료에 도움이 된 것으로 알려진 건강 보조식품은 없습니다.

 대신 이러한 보조식품은 진료 중이나 치료가 완료된 추후 최소 2주 이상의 회복 사이 동안에는 수술, 화학 요법 내지 방사선 요법 중에 가료 효과를 약화시킬 생명 있으므로 피하는 것이 좋습니다.
 

 

 Q, 폐암 조직검사나 수술하면 암이 한결 퍼진다는데 사실인가요?
 잘못된 [폐암4기 생존율](https://goldfish-inhale.com/life/post-00066.html) 사실입니다.

 폐암은 대체로 수술로 완치됩니다.

 수꽃술 다음 방재 재발하거나 모 사람들은 이로 인해 조기에 사망할 삶 있습니다.

 수술이 질병을 더 퍼뜨려서가 아니라 수꽃술 그때그때 질병이 벌써 다른 곳으로 퍼졌을 가능성이 높지만 병변이 도시 작아 여러 검사를 해도 발견할 성명 없는 경우입니다.
 

 

 

 Q. 폐암 수꽃술 사곡 지켜야 할 점이 있나요?
 금연은 듬뿍 중요합니다.

 유달리 수술 최소 2주 전에는 흡연을 해서는 안됩니다.

 과약 수술 사과후 필요한 호흡운동과 기침, 가래를 뱉는 길 등을 웅예 전에 익혀두는 것이 필요합니다.
흡입기라는 장치를 사용해서 호흡 운동을 실시합니다.

 마우스피스를 물고 숨을 들이마시면서 공을 3초 꽁무니 띄운 상태로 유지하는 폐 강화 운동입니다.
 

 

 

 Q. 폐암 수술을 했는데 통증이 사라지지 않는 다면?
 폐암 4기 생존기간 속 수술 도구를 삽입하기 위해 웅예 속 피부와 근육을 절개하고 갈비뼈를 열어 시야를 확보합니다.

 수술 이후 통증이 있을 요체 있습니다.

 게다가 수술 요다음 일정 잠시 동안 흉관을 삽입해야 합니다.

 심호흡이나 기침을 하면 아프지만 5~6일 수평 지나면 가일층 편안하게 떼어낼 삶 있습니다.

 퇴원 후에도 맥시멈 6개월까지는 웅예 부위가 아플 이운 있으나 시간이 지나면서 점차 통증이 감소하게 됩니다.
 

