---
layout: post
title: "아이가 학교폭력을 당하는 상황인지 확인하고 싶다면? (학교폭력의 유형)"
toc: true
---

 안녕하세요.
 ​
 해드림 행정사 사무소입니다.
 ​
 오늘은 학교폭력이라고 규정지을 생목숨 있는 유형들을 설명드리고자 합니다. 많은 아이들이 학교에서 다양한 형태의 학교폭력을 경험하고 있습니다. 약하게는 놀림에서부터 강하게는 물리적 폭력이나 현금 갈취 등 그대 형태가 너무나도 다양하고 어디서부터 학교폭력이라고 규정짓기가 어렵기에 학교폭력 가이드라인에서 정하고 있는 유형을 기이 알려드리려고 합니다.
 ​
 학교 폭력의 세목 및 예시
 ​
 ​
 1. 신체 폭력
 신체폭력은 가옹 일반적인 학교폭력의 유형이라고 할 운명 있습니다. 영별히 여성 학생들보다는 남아 학생들이 해당되는 경우가 많으며, 대컨 장난을 친다는 변명으로 가해 학생들이 처벌을 피하려고 하나, 가이드라인에서도 규정되어 있듯이 학교에서는 사소한 괴롭힘, 학생들이 장난이라고 여기는 행위도 학교폭력이 될 성명 있음을 인식할 요행 있도록 분명하게 가르치고 있습니다.
 ​
 * 신체를 손, 발로 때리는 등 고통을 가하는 행위(상해, 폭행)
 *일정한 장소에서 쉽게 나오지 못하도록 하는 행위(감금)

 *강제(폭행, 협박)로 일정한 장소로 데리고 가는 행위(약취)

 *상대방을 속이거나 유혹해서 일정한 장소로 데리고 가는 행위(유인)

 *장난을 빙자한 꼬집기, 때리기, 똑 밀치기 등 상대학생이 폭력으로 인식하는 행위

 ​
 ​
 2. 말 폭력
 언어폭력은 가해 학생의 의도와는 관련 없이 타격 학생이 모욕감, 두려움을 느꼈다면 학교폭력이라고 할 복운 있습니다. 워낙 SNS 상에서 누 학생에 대한 뒷담화 등을 할 경우, 댁네 사실이 진실이라 하더라도 범죄이며 허위인 경우에는 형법상 가중 처벌의 대상이 됨을 인지해야 합니다.
 ​
 * 여러 사람 앞에서 상대방의 명예를 훼손하는 구체적인 말(성격, 능력, 상태 등)을 하거나 그런 내용의 글을 인터넷, SNS 등으로 퍼뜨리는 행위(명예훼손)
 * 여러 성격 앞에서 모욕적인 용어(생김새에 대한 놀림, 병신, 바보 등 상대방을 비하하는 내용)를 지속적으로 말하거나 그런 내용의 글을 인터넷, SNS등으로 퍼뜨리는 행위(모욕)

 * 신체 등에 해를 끼칠 듯한 언행(“죽을래” 등)과 문자메시지 등으로 겁을 주는 행위(협박)
 ​
 ​
 3. 금품 갈취(공갈)
 금품 갈취의 선례 많은 케이스들이 짓 일진이라고 일컬어지는 가해 수강생 무리가 해 학생의 핸드폰,에어팟 등 가격 전자제품등을 빌려가 돌려주지 않는 경우가 많습니다. 이런 정황 더구나 금품 갈취로 학창 폭력에 해당되므로 주의해야 합니다.
 ​
 * 돌려 줄 생각이 없으면서 돈을 요구하는 행위

 * 옷, 문구류 등을 빌린다며 되돌려주지 않는 행위

 * 일부러 물품을 망가뜨리는 행위

 * 돈을 걷어오라고 하는 행위
 ​
 4. 강요
 * 속칭 빵 셔틀, 와이파이 셔틀, 주제 대행, 시합 대행, 심부름 강요 등 의사에 반하는 행동을 강요하는 행위(강제적 심부름)

 * 폭거 내지 협박으로 상대방의 권리행사를 방해하거나 해야 할 의무가 없는 일을 하게 하는 행위(강요)
 ​
 ​
 5. 따돌림
 해드림 행정사로 문의주시는 분들 거의거의 대부분이 따돌림으로 인한 자녀의 학교폭력 경험을 토로하시곤 합니다. 그만치 따돌림은 케이스들이 너무나도 많고, 사춘기 자녀들의 정신적인 고통이 큰 경우가 많습니다. 심한 경우, 아이가 등교를 거부하는 등 배움터 폭력으로 인해 미인정결석 등 근태관리에 문제가 생겨 차후 단과대학 진학 소리 이로 인해 문제가 생기는 경우가 많습니다.
 ​
 * 집단적으로 상대방을 의도적이고, 반복적으로 피하는 행위
 * 싫어하는 말로 바보 처리 등 놀리기, 빈정거림, 면박주기, 겁주는 행동, 골탕 먹이기, 비웃기
 * 다른 학생들과 어울리지 못하도록 막는 행위
 ​
 6. 성폭력
 * 폭행·협박을 왜 성행위를 강제하거나 유사 성행위, 성기에 이물질을 삽입하는 등의 행위
 * 상대방에게 폭행과 협박을 하면서 글 모멸감을 느끼도록 신체적 접촉을 하는 행위
 * 성적인 말과 행동을 함으로써 상대방이 열매 굴욕감, 수치감을 느끼도록 하는 행위
 ​
 ​
 7. 사이버폭력
 * 속칭 사이버모욕, 사이버명예훼손, 사이버성희롱, 사이버스토킹, 사이버음란물 유통, 대화명 테러, 인증놀이, 게임부주 강요 등 [과제 대행](https://imagetojpg.com/life/post-00084.html) 정보통신기기를 이용하여 괴롭히는 행위

 * 특정인에 대해 모욕적 언사나 욕설 등을 인터넷 게시판, 채팅, 카페 등에 올리는 행위. 특정인에 대한 저격글이 그쪽 테두리 형태임

 * 특정인에 대한 허위 글이나 개인의 사생활에 관한 사실을 인터넷, SNS 등을 통해 불특정 다수에 공개하는 행위

 * 나머지 수치심을 주거나, 위협하는 내용, 조롱하는 글, 그림, 동영상 등을 정보통신망을 통해 유포하는 행위

 * 공포심이나 불안감을 유발하는 문자, 음향, 용자 등을 휴대폰 등 정보통신망을 통해 반복적으로 보내는 행위
 ​
 학창 폭력의 개념은 규정하는 바가 끔찍스레 방대합니다. 배움터 내·외에서 학생을 대상으로 발생한 상해, 폭행, 감금, 협박, 약취·유인, 명예훼손·모욕, 공갈, 강요·강제적인 심부름 및 성폭력, 따돌림, 사이버 따돌림, 정보통신망을 이용한 음란·폭력 정보 등에 의하여 신체·정신 또 재산상의 피해를 수반하는 행 등을 일컫는데, 위의 유형들은 예시적으로 열거한 것으로, 신체·정신·재산상의 피해를 수반하는 모든 행위는 학교폭력에 해당합니다.
 ​
 상관 판례에서는 (서울행정법원 판례 2014구합250 판결) ‘학교폭력은 폭행, 명예훼손·모욕 등에 한정되지 않고 이와 유사한 행위로서 학생의 신체·정신 내지 재산피해를 수반하는 모든 행위를 포함한다.’고 여 범위를 폭넓게 인정하였습니다.
 ​
 배움터 폭력은 타격 학생에게 그지없이 씻을 고갱이 없는 상처를 남기는 엄중한 사안입니다. 이에 학원 폭력으로 인한 피해를 당하셨다면, 전기 단계부터 정확하고 자세한 안내를 통해 사건을 진행하셔야 합니다. 해드림 행정사 사무소에서는 교육계에 오랫동안 몸담은 경력을 바탕으로 학교폭력대책 심의위원장 및 심의위원을 두루 거친 전문가들이 사안을 담당하고 있습니다. 또 억울한 학교 완력 신고로 인하여 자녀가 조치를 받게 되었다면, 행정심판을 거쳐 네년 두량 결정을 뒤집는 것도 가능합니다. 무엇보다도 중요한 것은 해당 분야의 전문가와 다름없이 진행하셔야 자녀의 억울한 사정을 백분 반영할 요체 있다는 것입니다.
 ​
 학교폭력으로 인해 고통받고 계시다면, 주저하지 마시고 해드림 행정사무소로 연락주시기 바랍니다.
 ​
 언제든 의비 고객님의 사건을 기미 일처럼 성심성의껏 상담해드리겠습니다.
 ​
 ​
