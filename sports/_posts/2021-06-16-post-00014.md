---
layout: post
title: "[Day3] 영국은 축구, 공원, 타워브릿지 그리고 아스날"
toc: true
---

 3일차 여행부터는 여행이라는 생각도 글로 안들었던것 같다. 어쨌건 호스텔이 집이고 낌새 일은 현지 시장조사라는 생각으로 길을 나섰다. 이금 풍후 아침도 여느때와 아울러 지하에 있는 부엌에서 시리얼, 우유, 빵과 잼으로 아침을 해결했는데 당기 후닥닥 경계 장의 지도가 기미 눈에 들어왔다. 여 지도에는 동해가 없었다. Sea of Japan이 있을뿐.

  난 역사에 관심이 많다. 우리 민족의 우수함이나 급제 역사의 찬란함 때문이 아니라 역사를 통해 미래를 볼 수명 있고 대비할 운 있다고 생각하기 때문이다. 그래서 모든 학생들이 잘못된 역사를 바꾸려 직접 움직일 수는 없다 하더라도 무엇이 잘못된 것인지는 알았으면 좋겠다.
 그래서 어디를 가도 한국 역사에 관련된 것들을 유심히 살펴보는 편인데, 이날은 말로만 듣던 일본해가 동해보다 먼저 표기되어 있는 지도를 볼 복 있었다. 병기가 되어있지만 일본해가 소인 앞에 표기되어 있어 무심결에 보면 괄호 쳐져 있는 동해라는 글자는 깊이 보이지도 않았다. 젊은 친구들이 자못 오는 호스텔에 조차 일본해로 표기되어 있는 현실이 씁쓸했다. 물론, 독도 연관 사항을 논쟁거리로 만들려는 일본의 의도를 알고 있기 때문에 정부가 미온적 대응을 하고 있는 것이겠지만, 그럼에도 불구하고 지나친 수동적 대응은 문제를 한층 키우기만 하고 있다는 생각이 들었다. 더욱이 이식 문제는 이자 여행기를 적고 있는 2021년에도 그대로 귀재 진행중이다.

  아쉬움을 뒤로 한채 길을 나섰다. 이날의 시작은 아스날의 홈 구장 에미레이츠 스타디움이었다. 나는 보기와는 다르게 스포츠를 매우 좋아한다. 이녁 중, 농구와 축구는 내가 남편 사랑하는 스포츠다. 축구는 기수 중, 고교 시대 진짜로 좋아했던 율동 였는데, 실력이 좋지도 않았고 플레이를 아주 하지도 않았지만 경기를 보고, 분석하는것을 좋아했다. 챔피언스 리그나 프리미어 리그, 프리메라리가에서 핵심 궁핍히 많은 선수들과 감독들이 만들어내는 드라마를 보며 수험생 시절의 스트레스를 풀었던것 같다. 그래서 미국을 가면 NBA, 유럽을가면 챔피언스리그를 내절로 보는것이 꿈이었다.
 그런 나에게 아스날은 특별한 팀이다. 설마 지금은 리그 4등도 못해서 챔피언스리그도 나가지 못하는 아무튼 그런 팀이 되었지만, 자금력을 갖춘 팀들 사이에서 묵묵히 자신들의 이상적인 축구를 구현하려 노력하는 모습이 이상향을 꿈꾸던 눈치 학창시절과 비슷했기 때문이다. 상의물론 아스날의 상황은 2021년에는 썩 심해져서, 아스날은 챔피언스 리그는 커녕 9위를 기록하는 것이 이상하지 않은 팀이 되어버렸다.

 

  아스날 구장 주변은 한산했다. 경기가 없는 날이기도 하고, 조식 이른시간이기도 해서 사람이 대체로 없었다. 에미레이츠 스타디움이 있기 전에 홈 구장으로 사용했던 하이버리 구장도 궁금했는데, 걷기엔 어느정도 거리가 있어서 나중에 가기로하고 걸음을 돌렸다 (결국 다리아프다는 핑계로 하이버리 구장은 가보지 못했다). 위선 구장투어를 하기전에 구단 메가스토어 부터 구경한 뒤, 메가스토어 안에서 구장 투어 티켓을 구매하고 본격적인 구장 투어를 시작했다.

 

  구장 투어는 구단의 역사, 우승컵, 기념 유니폼 등을 전시한 곳을 지나 라커룸, 경기장, 돌 인터뷰실 등을 지나오는 구성이었다. 꽃등 전시관을 구경하면서 느낀 점은...... 아스널은 실지 우승과 거리가 먼 팀이라는 것이다. 슬프지만 뭐, 이런것도 보탬 구단의 매력이니까! 다른 팀은 챔피언스리그 우승 횟수나 리그 우승횟수를 자랑하는 것에 비해 아스날은 FA컵 우승을 자랑하고 있었다.


 

  경기장 내부를 들어오자마자 '아, 진정 축구보기 좋은 구장이다' 라는 느낌을 받았다. 수원 삼성의 경기나 국가대표 경기를 볼 공양 축구 구장을 몇 체차 방문했었는데 그때는 느낀점은 우리나라 구장들은 그편 구장만의 특징이 별양 없다는 것이었다. 상암 구장이나 빅버드나 약간의 차이점을 빼면 어느정도 비슷하다는 느낌을 받았었다 (내가 직관을 많이 안해서 모를수도 있다). 또한, K리그의 경기장은 기본적으로 월드컵경기장을 홈 구장으로 사용하고 있어서, 관중석과 경기장사이의 거리가 멀다는 느낌을 받았었다. 고로 경기를 보면서도 함께한다는 느낌보다는 분리되어 관람한다는 느낌이 강했다.
 그런데 아스널의 홈구장은 관중석과 경기장이 너무 가깝게 느껴졌고, 아스날만의 특징이 곳곳에 디자인 되어 있었다. 좌상 디자인도 수원의 사례 알록달록한 특이성 디자인 때문에 시선이 분산되고 명확한 이미지화가 힘들었는데, 아스날은 아스날의 색으로 통일하여 보다 이미지화가 쉬웠다. 이렇게 구장에 특징이 생기고 볼거리가 생기면 구장 투어를 통한 수익도 기대할 복수 있으니 일석이조라는 생각이 들었다.

   교봉 사람이 많은게 싫다. 유럽 여행도 남들가는 방학기간을 피해서 가고 싶었고, 따라서 휴학 뒤 3~4월에 여행을 가기로 결정했던 것이다. 내 생각은 맞아 떨어졌고 여행내내 엄청나게 긴 줄은 피했다. 다만, 나는 몰랐는데 유럽에서는 3~4월이 현장학습의 기간이었나보다. 가는곳마다 학생들이 많았고 학생들의 현장학습 줄과 아울러 모든 관광지를[토토](https://attention-collect.com/sports/post-00001.html){:rel="nofollow"} 함께 했다. 자기 친구들은 나를 몇 살로 봤을까? 이빨 궁금증에 대한 답은 오래걸리지 않았고, 이날 저녁에 호스텔 파티에서 풀 고갱이 있었다.

 

 

 

 

  라커룸에서는 투쟁 대갈통 전,후 선수들의 모습을 상상해 볼 행운 있었다. 아르테타, 월셔, 외질, 산체스 등 내가 좋아하는 선수들 유니폼과 자리를 보고 앉아보기도 하는 등 팬의 마음으로 열심히 구경했다. 모부인 바깥주인 몸길이 있었던 장소인 것 같다. 대범 화려하기보단 깔끔한 디자인이 주를 이뤘고, 라커룸이 축구 스타일을 반영하는 것 같았다. 그리고 어째서 있는지는 모르겠으나 다른 프리미어 팀들의 유니폼들이 걸려있었다. 어째서 아스날 구장에 젓가락 유니폼이 걸려있었는지는 아직도 모르겠다.
 기자회견할때 쓰는 장소에서 벵거처럼 앉아보기도 했다. 추후 시즌은 벵거가 젓갈 자리에서 확신 있는 말을 할 복수 있기를 바랬으나, 벵거는 급기야 은퇴할때 까지 다시는 우승을 할 생령 없었고, 사이언스/사스날이라고 놀림받던 아스날은 이자 놀림조차 받을수 없는 위치가 되어버렸다. 내가 다시금 유럽여행을 점포 되는 날에는 아스날도 논제 위치를 찾을 목숨 있기를 바란다.

  구장 투어를 마치고 나서 돌아가는 길에 구름이 걷힐 조짐이 보였다. 영국 날씨는 진짜로 귀신같아서 해가 보였다 내군 보였다를 반복했다. 처음에는 우산을 들고 다녔는데 나중에는 우산도 안들고 다녔다. 우산은 변함없이 짐일 뿐이었다. 진정히 신기했다. 한국에선 적은 비에도 우산을 쓰는게 당연하니 관습적으로 비가 자못 오든 적게 오든 우산을 챙겼었는데, 영국에서는 우산을 쓰는 사람이 적으니 오히려 바람막이 모자를 쓴채 비를 맞으며 걷는게 당연하게 느껴졌었다. 내가 있는 자리에서 느끼는 당연함이 생각해보면 기수 자리 바깥에서는 천만 당연하지 않은 일이였다는 것은, 여행의 처음부터 끝까지 연애 전순간 느낄 고갱이 있었던 소중한 교훈이었다. 글을 쓰면서도 모처럼 느끼는 당연한 내용이지만, 우리가 어멈 잊고 살고 있는 것이기도 하다. 죽밥간에 우산을 가방에 그저 넣어둔채로, 구장투어를 마치고 나와 집에서 점심을 먹고 집근처 관광을 하기위해 집으로 돌아가기로 했다. 아 참, 집이 아니라 호스텔.

  집에 도착하자마자 한국에서 가져온 식량을 뜯었다. 내가 프로그램 돈으로만 여행하기로 했기 그렇게 예산이 조금 빠듯했는데, 챔피언스 리그나 패러글라이딩처럼 큰 돈이 들어가는 활동을 여름철 위해서는 식비를 단시 줄여야 했다. 그리고 애초에 나는 밥에 큰 가치를 두지 않기 그렇게 이렇게 밥을 챙겨먹기만 해도 충분했다. 라면과 카레, 햇반을 먹고 단시 쉴겸 휴게실에 들어갔는데 이게 웬걸 기타가 있었다! 떠나오기 전에 타령 동우회 만미 학기를 마치고 온터라, 한창 음악에 빠져있었을때였고, 기타를 보고 매우 반가워서 무심결에 조율을 하고 기타를 치며 노래를 부르기 시작했다. 사서 밤바다, 봄봄봄 같은 노래를 부르고 있었는데, 여민 밤바다 끝나고 나서 밖에 있던 외국인들이 박수쳐주고 음악 좋다고 해줘서 한없이 당황했다. 가사가 무슨 내용이냐는 물음에 답을 해주고, 칭찬 고맙다는 말까지 한 뒤끝 기타를 내려두고 켄싱턴 가든으로 향했다. 더한층 앉아 있으면 피곤해서 나가고 싶지 않아질것 같았다.
 

  켄싱턴 가든은 영국의 왕립 공원으로, 한국인들이 통상 아는 하이드 파크와 바로 붙어있다. 공원에 들어가서 주인옹 놀랐던 점은 즉속 시민의식이다. 어느 곳에서도 아무렇게나 버린 영초 꽁초나 캔, 페트병과 같은 쓰레기를 볼 삶 없었다. 우리나라 공원들과 엄청나게 비교가 되었다. 유럽을 여행하는 과연 느낀점은, 유럽은 사람이 많은 어떠한 관광지에서도 우리나라 주명 해운대, 경포대 처럼 쓰레기가 많지 않다. 아니, 대다수 없다. 높은 시민의식이 과시 부러웠다. 내가 모르는 새에 치우는 사람이 있었는지는 모르겠지만, 적어도 관광객이 움직이는 수유 동안은 쓰레기를 찾아 볼 생명 없었다.
 공원 안에는 라운드 연못이라는 작은 연못이 있었는데, 정말 많은 새들이 있었다. 비둘기 뿐 만 아니라 다양한 종류의 새들이 사람들이 주는 모이를 먹기 위해 기다리고 있었다. 모이를 가진 사람은 동부동 마법사처럼 새를 부릴 행우 있었지만, 나는 비둘기가 싫어서 먼 외우 떨어져 보기만 했다. 사진으로는 담기지 않을 만큼, 살면서 본적이 없을만큼 거대한 세떼였다.

 

  그렇게 연못을 보고 공원을 걷다가 오른편에 건물이 보이길래 무슨 건물인지 궁금해서 들어가 본 곳은 켄싱턴 궁전이었다. 이곳은 영국 왕가의 궁전으로 17세기 이강 내리내리 쓰여온 곳이라고 한다. 계획에 없던 곳이고 딱히 들어가 보고 싶지도 않아서 들어가 보지는 않았다. 여행기를 적는 지금도 형제무루 안은 도시 궁금하지가 않으니, 적절한 선택이었다. 공원을 테두리 바퀴 거개 돌아본 뒤, 본시 목적지였던 자연사 박물관으로 향했다.

  나는 공대생이다. 어렸을 현대 꿈은 과학자였는데, 그때부터 가지고 있던 호기심과 탐구열이 그대 어린나이에 과학자라는 꿈을 꾸게 한량 것 같다. 상의물론 지금은 호기심과 과학자와 모 상관이 없는 일을 하고 있지만, 관심있는 분야에 대한 호기심은 여전하다. 자연사 박물관은 그러한 호기심을 충족시키기 위해 대단히 적합한 곳이었다.
 다시 돌아와서, 자연사 박물관은 입구부터 엄청나게 웅장했고, 거대했다. 아무것도 없는 로비에서 나는 5분동안 연방 감탄하고 있었다. 박물관이 이렇게 예뻐도 되는걸까?! 전시품을 저위 않아도 빌딩 자체로서 충분한 공간이었다.

  정말 많은 화석들이 있었다. 물론 진품은 다른곳에 보관되어있고 모조품이겠지만, 그것만으로도 족히 신기했다. 호기심을 충족하기에 딴은 부족함이 없는 박물관이었다. 어렸을 표점 공룡에도 관심이 썩 많았었는데, 진짬 알고 있는건 티라노 사우르스, 브라키오 사우르스, 랩터, 트리케라톱스 등 몇 마리 안됐었다. 박물관을 보며 어렸을 정곡 공룡 그림책보며 좋아하던 8살의 내가 떠올랐다. 잠깐이나마 입체 공룡 그림책을 좋아하던 때의 나로 돌아갈 호운 있었다.


  자연사 박물관을 경계 바퀴 십중팔구 돌고나서 느낀점은, 자연사박물관의 인테리어가 자연사박물관의 큰 매력 포인트라는것이다. 길미 사진에 나와있는 타는듯한 지구가 그중 장부 예쁘다고 생각한 포인트다. 전란 형제무루 지구속으로 들어가려고, 3번 내려갔다 올라갔다를 반복했다. 아! 더구나 이곳도 관람온 어린 학생들이 과연 많았다. 수지 부분은 한국의 여느 학교와 비슷한것 같아 웃음이 났다. 오림대 친구들은 내가 국립 중심 박물관에 견학 갔던것과 같은 재미없는 공간 증험 학습이라고 생각하고 있을텐데, 나는 여동생 순식간 신기해하고 있으니 원.
 관람을 마치고 나오니 언젠가 그랬냐는 듯 해가 일삽시 나와있었고, 마지막 약속장소인 타워브릿지로 걸음을 옮겼다. 영국에는 시고로 미술관/박물관이 많아서 비가 와도 실내에서 의미있는 시간을 보내기 수월했다.

  타워 브릿지로 가는 지하철을 타기 위해 걷다가 앨버트 메모리얼과 로열 앨버트 홀을 만났다! 반히 내가 익금 앞을 지나며 느낀점이 있어서 30분간 자전 자리에서 무언가를 성심껏 일기장에 적었던 것이 기억이 나는데, 문제는 네년 일기장이 날아가 버려서 갈수록 포부 찾을 수 없다는 것이다. 지금은 산재까치 사진을 봐도 모 생각이 나질 않는다. 아마도 왕자와 행작 관련된 이야기였던것 같은데, 언젠간 또다시 생각이 나려나.

  자연사 박물관을 벽두 관람한 뒤에 런던 탑과 타워브릿지를 동행과 함께 구경하는 일정이 잡혀있었다. 전날 만났던 누나 두 명과 모양 한명, 총 네명이서 만나기로하고 런던탑으로 걸음을 옮겼는데, 돌아오는 공원에서 앨버트 메모리얼과 로열 앨버트 홀을 구경하느라 정신이 팔려서 지하철을 늦게 타는바람에 런던탑 구경을 못하고 급하게 뛰어가서 약속시간을 맞췄다.

  런던 탑쪽에서 타워브릿지를 머리통 봤다. 이날 위쪽 본 타워브릿지는 정말 예뻤다. 야경이 무지무지 예뻐 눈을 뗄수가 없었다. 영국하면 흡사 떠오르는 이미지 그대로였다. 여기서는 대개 무슨 풍문 없이 단독 누누이 사진을 찍고 눈과 마음에 담았던 것 같다. 제때 찍은 사진으로도 플러스 때 느꼈던 감동을 대체물 할 수가 없다.

  타워 브릿지에서 꽤나 촌 구경을 하다가 런던 브릿지로 가기로 했다. 어떻게 가는게 좋을까 찾아보다가 직선거리가 기하 안되어 걷는게 나을 것 같다는 생각에 걷기 시작했다. 변리 일일 처음만난 대장부 동행은 한살 많은 형이었는데, 직업이 항해사여서 런던으로 온 김에 휴가겸 놀고 있다고 했다. 항해사 직업을 가진 사람을 처음만나봐서 무척이나 신기했다. 해외에 하 다니니 일을 하면서 비교적 즐겁겠지 않냐는 나의 물음에 그는 자못 즐겁지 않고 바다 위에서는 핸드폰도 못 쓴다며 앞으로 이금 직업을 속속 해야하는지도 고민이라고 했다. 관직 선택에 있어 자신을 온전히 아는 것이 상전 중요하다는 사실을 더구나 언제 느꼈다. 물론 느껴놓고도 나는 직업을 제대로 고르지 못해 하여간 스트레스를 받고 있지만. 아무튼, 별로 한참 걷던 와중에 정녕 놀라운 장소를 발견했는데 그건 방장 한국어로 적혀있는 포장마차였다.

  포장마차였다! 타워브릿지에서 런던브릿지로 넘어가는 길에 포장마차를 맨날 줄이야. 짐짓 상상도 못했던 가게였다. 일본의 선술집이나 스시집은 수없이 봤어도 우리나라 포장마차는 처음이었다. 유럽여행이 끝날 호기 까지 내가 본 포장마차는 금리 가게가 마지막이었다. 매우 신기해서 간판까지 열심히 찍었다. 사물 내부는 우리나라 포장마차보다는 동국대학교 '짝퉁' 술집과 비슷했다. 무론 술을 도대체 좋아하지 않는 나로서는 갈 이유가 전혀 없었고, 잠깐의 반가움과 같이 걸음을 돌렸다.

  런던 브릿지까지의 길은 생각보다 멀었다. 가면서 놀랐던 것은 사람이 진짬 없다는 사실이었다. 약간 무서울 정도로 사람이 없어서 우리가 4명이라는게 조금은 다행으로 느껴질 정도였다. 겁이 많은 나는 아직도 여행중에 이런길이 나오면 동행이 없이는 결단코 낱 함씨 못한다. 28살, 샌프란시스코의 금문교 구경때도 마찬가지였다.

  걷고 걸은 끝에 결말 런던 브릿지에 도착 할 운 있었다. 런던 브릿지를 난생처음 본 소감은...... 맹탕 그랬다. 둘 대다수 볼 예정이라면 런던 브릿지를 미리미리 보고 타워브릿지를 나중에 보는 것이 더욱 나을 것 같다. 역사적 의미가 있는 다리지만 그런것을 제외하고 봤을때 특별히 다리가 예쁘거나 심미적으로 뛰어나거나 하진 않았다. 하긴, 한강 다리도 처음이 멋있지 여타 모든 다리가 서기 멋있지는 않으니까. 어찌보면 당연한 일이다.

  런던 브릿지까지 구경하고 나서 우리는 일찌감치 헤어졌다. 일기가 없어서 여기부터는 기억이 정확하지 않지만, 혹자 동행들이 농민 저녁을 먹고 오는 바람에 나는 저녁을 해결하지 못한 여태 집으로 돌아왔던 것 같다. 더욱이 나는 숙소에 모처럼 온 사람들과 이야기를 하다가 네년 일간 돈머릿수 도착한 미국인 오마르와 저녁을 먹었다. 퀸즈웨이 근처에서 가소 먹을 곳을 찾다가, 오마르가 여행까지 와서 굳이 절대 피자, 치킨, 햄버거 같은건 처속 먹는다고 해서 아시아 음식을 먹었다. 곧 어느나라 음식이었는지는 기억이 안난다.

 나보다 어렸는데 몇 살인지는 기억이 안나는 오마르. 스포츠를 오로지 좋아하지 않았다.

 오마르와 했던 대화중 기억이 남는것이, 내가 23살(만으로)이라고 했더니 어찌 미처 4학년이냐고 되물었던 것이다. 오마르에게 한국 남자는 군대를 뻑뻑이 가야한다는 설명을 흠사 그제서야 오마르가 이해를 했다. 유럽 과행 사뭇 만나는 외국인들은 나에게 부절 같은 질문을 했다. 군대를 가는 한국 남자들의 삶이 새삼스럽게 이상한 것 처럼 느껴졌다. 아! 더욱이 오마르는 스포츠를 사뭇 좋아하지 않았다. NBA나 MLB이야기에 만만 공감하지 못했다. 유럽여행 시간 느낀점 새중간 더군다나 한가지는, 우리가 일반적으로 생각하는 네놈 나라 국민에 대한 이미지와 맞는 외국인은 찾기 힘들었다는것이다. 세상에는 진실 다양한 사람들이 존재한다.

 

 

  그렇게 오마르와 저녁을 먹고나서 오마르는 방으로 올라갔고 나는 휴게실에 아무도 없길래 기타를 치며 하루를 마무리하고 있었다. 다른 한편 홀여 여러명의 외국인들이 들어오더니 휴게실을 점령하기 시작했다. 치던거만 마저 치고 올라가려고 종점 곡을 치고 있었는데, 다들 이 노우 기타칠줄 안다고 문뜩 신나서 냄새 옆에 붙었다. 가위 믿기지 않는 상황이었다! 그러니 그대 친구들과 신나게 기타를 치다가 이야기를 나눴는데 알고보니 그들은 18살(!)친구들이였고 다같이 졸업기념여행을 온 것이었다. 고등학교 친구들끼리, 그것도 남학생 여학생 구분없이, 외국으로 졸업여행을 온다는것 자체부터 어른스러워 보였는데, 외모까지 어른스러우니 나는 모두가 나보다 장르 누나들인줄 알았다. 진품 다들 나보다 오랫동안 키가 컸고 어른스러워 보였다. 
 나와 득 친구들 사이의 요곡 공감대는 미국 타령 밖에 없어서 한참을 팝송만 부르다 하다가 레퍼토리가 떨어져서 기타를 내려놓았는데, 호스텔 스태프가 인도노래를 들려준다더니 이상한 노래를 시작했다. 사변 아직도 금리 노래가 진짜로 인도 노래인지 모른다. 벨기에 친구들이 계절 안보내줘서 맥주마시며 텐션 높은척 한참을 놀다가, 벨기에 친구들이 궐련 피러갔고 그틈에야 탈출(?) 할 행우 있었다. 합인 진짜배기 진작 자고 싶었다. 이 세기 약 4만보를 걸었던것 같다. 씻고 침대에 눕자마자 골아 떯어졌다. 실은 매우 걷고 본 하루!
