---
layout: post
title: "[NBA] 2021.01.01 제임스, NBA 첫 1천경기 연속 11점 이상 달성…36세..."
toc: true
---

샌안토니오 해먼 코치, 매니저 퇴장에 '첫 딸 사령탑 데뷔'
 ​

 ​
 미국 프로농구(NBA) LA 레이커스의 '킹' 르브론 제임스가 36세 생일에 게다가 하나의 금자탑을 세웠다.
 

 제임스는 30일(미국시간) 미국 텍사스주 샌안토니오의 AT&T 센터에서 열린 샌안토니오 스퍼스와의 2020-2021 NBA 정규리그 한설날 경기에서 26점 8어시스트 5리바운드를 올려 레이커스의 121-107 승리에 앞장섰다.
 

 유달리 이날 36번째 생일을 맞이한 그는 2쿼터 제한 6분 15초 전 훅슛으로 11점째를 올려 NBA 역사상 최초로 '1천 활동 부절 두 자릿수 득점'의 주인공이 됐다.
 

 제임스는 2007년 1월 5일 밀워키 벅스를 상대로 8점을 올린 이후엔 14년 곁 빼놓지 않고 10점 이상씩을 쌓았다.
 

 2018년 3월 30일 뉴올리언스 펠리컨스와의 경기에선 867경기 유지 두 자릿수 득점으로 '농구 황제' 마이클 조던의 기록을 넘어 득 측면 NBA 역대 1위로 올라선 성적 쉼 없이 1천 다툼 고지까지 내달렸다.
 

 제임스는 "그저 순간을 살아가며 한층 나아지려고 노력해왔다. 내가 사랑하는 경기를 18년이나 할 삶 있는 건 축복받은 일이고, 계속 높은 수준의 경기를 보여주고 싶다"라고 소감을 밝혔다.
 

 제임스를 필두로 데니스 슈뢰더(21점), 앤서니 데이비스(20점 8리바운드) 등이 활약하며 승리를 챙긴 레이커스는 시즌 3승 2패를 기록했다.

 ​
 그레그 포포비치 감독이 2쿼터 한복판 퇴장을 당해 여성인 베키 해먼 코치가 벤치에 앉은 샌안토니오는 발단 2연승 종당 2연패에 빠졌다.
 

 해먼 코치는 여인 최초로 NBA 경기를 지휘한 계집 지도자로 이름을 남겼다.
 

 선수 세기 미국여자프로농구(WNBA)에서 6번 올스타에 뽑힌 해먼 코치는 NBA의 대표적인 아낙 지도자다.
 

 2014년 8월 NBA 샌안토니오 코치에 선임, 여성으로는 처음으로 NBA 구단으로부터 급여를 받는 정식 코치가 됐다.
 

 이듬해엔 여인네 최초로 서머리그 감독을 맡아 팀 우승 이끌었고, 2017년엔 부녀 최초로 시범경기를 지휘하기도 했다.
 

 뜻밖의 '사령탑 데뷔 전'을 치른 해먼 코치는 "선수들을 적절한 위치에 놓으려고 했고, 동기부여를 주려 했다"면서 "선수들과 이겨서 나갔더라면 보다 좋았을 것"이라고 말했다.
 

 제임스는 "해먼 코치와 우리 리그에 온통 축하한다"라고 인사했고, '적장'으로 진통 프랭크 보겔 레이커스 감독도 "해먼은 준비되어 있고, 똑똑하다. 유족히 자격이 있다. 언젠가는 훌륭한 감독이 될 것"이라고 격려했다.

 

 

 LA 클리퍼스는 포틀랜드 트레일블레이저스를 128-105로 물리치고 2연승을 이어가며 서부 콘퍼런스 선두(4승 1패)를 달렸다.
 

 26일 스포츠 복판 권속 서지 이바카의 팔꿈치에 맞아 식솔 안쪽을 꿰매 두 경기를 결장했던 커와이 레너드가 돌아와 잘못 팀 최다인 28점을 올렸고, 폴 조지가 더블더블(23점 10리바운[토토](https://cactusselect.tk/sports/post-00001.html)드)을 작성했다.
 

 브루클린 네츠는 애틀랜타 호크스에 145-141 신승을 거두고 2연패에서 벗어나 시즌 3승(2패) 째를 거뒀다.
 

 케빈 듀랜트가 33점 11리바운드 8어시스트의 '트리플 더블 급' 활약을 펼쳤고, 카이리 어빙이 4쿼터에만 17점을 몰아넣는 등 25점 6리바운드로 승리를 이끌었다.
 

 ◇ 31일 NBA 전적
 

 보스턴 126-107 멤피스
 

 브루클린 145-141 애틀랜타
 

 마이애미 119-108 밀워키
 

 LA 레이커스 121-107 샌안토니오
 

 샬럿 118-99 댈러스
 

 LA 클리퍼스 128-105 포틀랜드
